kernel void init_cube(__global float3* position, __global float3* velocity, const int numParticlesPerLine) {
	size_t i = get_global_id(0);

	float distance = 0.75 / (float)numParticlesPerLine;
	
	position[i].x = 0.375 - ((i % numParticlesPerLine) * distance);
	position[i].y = 0.375 - (((i / numParticlesPerLine) % numParticlesPerLine) * distance);
	position[i].z = 0.375 - (i / (numParticlesPerLine * numParticlesPerLine)) * distance;

	velocity[i] = (float3)(0);
};