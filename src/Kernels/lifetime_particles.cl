kernel void update_lifetime(__global float4* position, __global float4* velocity, const float lifeTime, const int nbParticles, const int nbLimitedParticles, float4 worldPosition) {
	size_t id = get_global_id(0);

	for (int i = 0; i < nbLimitedParticles; i++) {
		if (position[nbParticles + i].w < 0.0f) {
			position[nbParticles + i] = (float4)(worldPosition.xyz, 0);
			velocity[nbParticles + i] = (float4)(0);

			position[nbParticles + i].w = lifeTime;
			break;
		}
	}
}
