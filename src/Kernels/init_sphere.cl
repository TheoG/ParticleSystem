kernel void init_sphere(__global float3* position, __global float3* velocity, const int nbParticles) {
	size_t i = get_global_id(0);

	float indice = i + 0.5;

	float phi = acos(1 - 2 * indice / nbParticles);
	float theta = 3.14159 * (1 + pow(5, 0.5)) * indice;

	position[i].x = (cos(theta) * sin(phi)) / 2.0f;
	position[i].y = (sin(theta) * sin(phi)) / 2.0f;
	position[i].z = cos(phi) / 2.0f;

	velocity[i] = (float3)(0);
};
