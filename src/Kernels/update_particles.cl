
kernel void update(__global float3* position, __global float3* velocity, __global float* forces, const float3 speed, const int nbForces) {
	size_t i = get_global_id(0);

	float3 acceleration = (float3)(0);
	
	for (int j = 0; j < nbForces; j++) {
		float3 force = (float3)(forces[(j * 4) + 0], forces[(j * 4) + 1], forces[(j * 4) + 2]);
		acceleration += normalize(force - position[i]);
	}
	velocity[i] = (velocity[i] + normalize(acceleration)) * 0.999f;

	position[i] += velocity[i] * speed;
};

kernel void updateLimitedParticles(__global float4* position, __global float4* velocity, __global float* forces, const float3 speed, const int nbParticles, const int nbLimitedParticles, const int nbForces) {
	size_t i = get_global_id(0);

	float3 acceleration = (float3)(0);
	
	for (int j = 0; j < nbForces; j++) {
		float3 force = (float3)(forces[(j * 4) + 0], forces[(j * 4) + 1], forces[(j * 4) + 2]);
		acceleration += normalize(force - position[i].xyz);
	}

	velocity[i].xyz = (velocity[i].xyz + acceleration) * 0.999f;

	position[i].xyz += velocity[i].xyz * speed;

	if (i >= nbParticles && i <= nbParticles + nbLimitedParticles - 1) {
		position[i].w -= 0.016f;
	}
};
