#ifndef OPENGL_CLASS_HPP
# define OPENGL_CLASS_HPP

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <string>
#include <vector>
#include <map>

class OpenGL {

public:
	OpenGL();
	~OpenGL();

	GLuint createBuffer(const std::string& bufferName, size_t size, int type);
	unsigned int loadCubemap(std::vector<std::string> faces);
	void bindBuffer(const std::string& bufferName);
	GLuint getVAO(const std::string& bufferName) const;
	GLuint getVBO(const std::string& bufferName) const;

	void finish();

private:

	std::map<std::string, GLuint> VAOs;
	std::map<std::string, GLuint> VBOs;

};

#endif