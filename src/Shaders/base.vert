#version 410 core

layout (location = 0) in vec4 position;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec3 gravityPosition;
uniform vec3 baseParticleColor;
uniform vec3 limitedParticleColor;
uniform int	 nbParticles;

uniform bool limitedParticles;

out vec3 ObjColor;

void main()
{
	gl_Position = projectionMatrix * viewMatrix * vec4(position.xyz, 1.0);

	float dMax = 2;
	gl_PointSize = 1.0;

	float d = distance(position.xyz, gravityPosition);

	
	if (limitedParticles) {
		ObjColor = limitedParticleColor;
		if (position.w > 0) {
			gl_PointSize = 2.0;
		} else {
			gl_PointSize = 0.0;
		}
	} else {
		ObjColor = vec3(clamp(baseParticleColor.x - clamp(d / dMax, 0, 1), baseParticleColor.x * 0.2, 1.0),
						clamp(baseParticleColor.y - clamp(d / dMax, 0, 1), baseParticleColor.y * 0.2, 1.0),
						clamp(baseParticleColor.z - clamp(d / dMax, 0, 1), baseParticleColor.z * 0.2, 1.0));
	}
}
