#ifdef WIN32
# include <windows.h>
#else
# include <OpenGL/CGLCurrent.h>
#endif

#include "OpenCL.hpp"

#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#if defined (__APPLE__) || defined(MACOSX)
static const char* CL_GL_SHARING_EXT = "cl_APPLE_gl_sharing";
#else
static const char* CL_GL_SHARING_EXT = "cl_khr_gl_sharing";
#endif

#include <glad/glad.h>
#include <GLFW/glfw3.h>


std::string getErrorString(int error);

OpenCL::OpenCL() {
	(void)CL_GL_SHARING_EXT;
	std::vector<cl::Platform> platforms;  
	cl::Platform::get(&platforms);  

	int err = 0;

	cl_ulong globalMemory = 0;

	// Iterates over platforms and devices and try to pick up the best one
	for (std::vector<cl::Platform>::iterator it = platforms.begin(); it != platforms.end(); ++it) {
		auto platform = cl::Platform(*it);

		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

		for (std::vector<cl::Device>::iterator it2 = devices.begin(); it2 != devices.end(); ++it2) {
			cl_ulong globalMemoryTmp = device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>();
			if (globalMemoryTmp > globalMemory) {
				globalMemory = globalMemoryTmp;
				this->platform = platform;
				this->device = cl::Device(*it2);
			}

		}
	}
	std::cout << "=============================================================" << std::endl;
	std::cout << "Device specs: " << std::endl;

	std::cout << "\tName: " << this->device.getInfo<CL_DEVICE_NAME>() << std::endl;
	std::cout << "\tType: " << this->device.getInfo<CL_DEVICE_TYPE>();
	std::cout << " (GPU: " << CL_DEVICE_TYPE_GPU << ", CPU: " << CL_DEVICE_TYPE_CPU << ")" << std::endl;
	std::cout << "\tVendor: " << this->device.getInfo<CL_DEVICE_VENDOR>() << std::endl;
	std::cout << "\tMax Compute Units: " << this->device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;
	std::cout << "\tMax number of work items: " << this->device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
	std::cout << "\tGlobal Memory: " << this->device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() << std::endl;
	std::cout << "\tMax Clock Frequency: " << this->device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << std::endl;
	std::cout << "\tMax Allocateable Memory: " << this->device.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>() << std::endl;
	std::cout << "\tLocal Memory: " << this->device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() << std::endl;
	std::cout << "\tAvailable: " << this->device.getInfo< CL_DEVICE_AVAILABLE>() << std::endl;
	std::cout << "=============================================================" << std::endl << std::endl;

	std::string version = this->platform.getInfo<CL_PLATFORM_VERSION>();
	std::cout << "OpenCL Version: " << version << std::endl;

#ifdef __APPLE__
	CGLContextObj	 kCGLContext	 = CGLGetCurrentContext();
	CGLShareGroupObj  kCGLShareGroup  = CGLGetShareGroup(kCGLContext);

	gcl_gl_set_sharegroup(kCGLShareGroup);

	cl_context_properties properties[] = {
		CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE,
		(cl_context_properties) kCGLShareGroup,
		0
	};
#elif _WIN32
	cl_context_properties properties[] =
	{
		CL_GL_CONTEXT_KHR, (cl_context_properties)wglGetCurrentContext(),
		CL_WGL_HDC_KHR, (cl_context_properties)wglGetCurrentDC(),
		CL_CONTEXT_PLATFORM, ((cl_context_properties)this->platform()),
		0
	};
#endif

	if (err != CL_SUCCESS) {
		std::cerr << "Context Creation: " << getErrorString(err) << std::endl;
	}

	this->context = cl::Context(device, properties, nullptr, nullptr, &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Context Creation: " << getErrorString(err) << std::endl;
	}
	this->commandQueue = cl::CommandQueue(this->context, this->device, 0, nullptr);
}

OpenCL::~OpenCL( void ) {
}


cl::CommandQueue OpenCL::getCommandQueue() const {
	return this->commandQueue;
}

cl::BufferGL OpenCL::createBufferFromOpenGL(const std::string& name, GLuint VBO) {
	int err = 0;

	cl::BufferGL bufferGL = cl::BufferGL(context, CL_MEM_READ_WRITE, VBO, &err);
	if (err != CL_SUCCESS) {
		std::cerr << "BufferGL Creation: " << getErrorString(err) << std::endl;
	}
	this->buffers[name] = bufferGL;
	return bufferGL;
}

cl::Buffer OpenCL::createBuffer(void* data, size_t size) {
	int err = 0;
	cl::Buffer b(this->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size, data, &err);
	return b;
}

void OpenCL::createProgram(const std::string& name, const std::string& kernelPath) {
	int err = 0;

	std::string fileString = readFile(kernelPath);

	cl::Program program = cl::Program(this->context, cl::Program::Sources(1, std::make_pair(fileString.c_str(), fileString.length())), &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Program creation: " << getErrorString(err) << std::endl;
	}
	err = program.build(std::vector<cl::Device>(1, device), nullptr);
	if (err != CL_SUCCESS) {
		size_t log_size;
		clGetProgramBuildInfo(program(), device(), CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		char *log = (char *) malloc(log_size);
		clGetProgramBuildInfo(program(), device(), CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		std::cerr << log << std::endl;
		std::cerr << "Program build: " << getErrorString(err) << std::endl;
	}
	this->kernels.insert(std::make_pair<>(name, cl::Kernel(program, name.c_str(), &err)));
}

cl::Kernel OpenCL::getKernel(const std::string& name) {
	return this->kernels.at(name);
}

cl::BufferGL& OpenCL::getBuffer(const std::string& name) {
	return this->buffers.at(name);
}

std::string	OpenCL::readFile(const std::string& filePath) {
	std::string kernelCode;
	std::ifstream kernelFile;

	kernelFile.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
	try {
		kernelFile.open(filePath);
		std::stringstream kernelStream;
		kernelStream << kernelFile.rdbuf();
		kernelFile.close();
		kernelCode = kernelStream.str();
	}
	catch (std::ifstream::failure e) {
		std::cerr << "ERROR: KERNEL::FILE_NOT_SUCCESFULLY_READ" << std::endl;
		exit (1);
	}
	return kernelCode;
}
