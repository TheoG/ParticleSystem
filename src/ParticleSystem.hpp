#ifndef PARTICLE_SYSTEM_HPP
# define PARTICLE_SYSTEM_HPP

#define PARTICLE_POSITION_BUFFER_NAME "particlePosition"
#define PARTICLE_VELOCITY_BUFFER_NAME "particleVelocity"
#define PARTICLE_LIFETIME_BUFFER_NAME "particleLifeTime"
#define SKYBOX_BUFFER_NAME "skybox"

#include "OpenGL.hpp"
#include "OpenCL.hpp"
#include "Shader.hpp"
#include "Camera.hpp"

#include <glm/glm.hpp>

#include "Color.hpp"
#include <algorithm>

#define MAX_GRAVITY_POINTS 2

class ParticleSystem {

public:
	ParticleSystem(Shader& shaderProgram);
	~ParticleSystem();

	void setParticleNumber(int number, int limitedParticles);
	int getParticleNumber() const;
	int getLimitedParticleNumber() const;

	glm::vec3& getGravityPosition(int index);
	std::vector<glm::vec3> getGravityPositions() const;
	
	glm::vec3 getParticleColor() const;

	void drawBuffer();
	void drawSkybox(Camera& cam) const;

	void updateParticlePositions();
	void updateLimitedParticlePositions();
	void createSphere();
	void createCube();
	void updateParticleLifeTime(glm::vec3 worldPosition);
	
	void setGravityPosition(int index, glm::vec3 position);
	void addGravityPosition(glm::vec3 position);
	void popGravityPosition();

	void setSpeed(float speed);

	void play();
	void pause();
	void toggleMouseTrack(bool toggle);
	bool isPlaying() const;
	bool isTrackingMouse() const;

	float getGravityDistance() const;
	void  setGravityDistance(float distance);

	void toggleRainbowMode(bool toggle);
	void setParticleColor(glm::vec3 color);

	void setLimitedParticleColor(glm::vec3 color);

	glm::vec3 mouseToWorld(Camera& cam, int width, int height, float xPos, float yPos);
	void computeGravityPosition(Camera& cam, int width, int height, float xPos, float yPos);

	float getLifetime() const;
	void setLifetime(float lifetime);

	void reset();

	enum Shape {
		Sphere,
		Cube
	};

	enum CameraMode {
		Cursor,
		Gravity,
		FreeCamera
	};

	void setShape(ParticleSystem::Shape shape);

	CameraMode getCameraMode() const;
	void setCameraMode(CameraMode camMode);

	static const float MinSpeed;
	static const float MaxSpeed;
	static const float DefaultSpeed;

	static const float MinLifetime;
	static const float MaxLifetime;
	static const float DefaultLifetime;

	static const int MinNbParticles;
	static const int MaxNbParticles;
	static const int DefaultNbParticles;

	static const int MinNbLimitedParticles;
	static const int MaxNbLimitedParticles;
	static const int DefaultNbLimitedParticles;

	static const int MaxGravityPoints;


private:
	OpenGL*	opengl;
	OpenCL*	opencl;
	Shader& shaderProgram;
	Shader*	skyboxShader;
	unsigned cubemapTexture;

	std::vector<glm::vec3> gravityPositions;

	Color*	particleColor;
	Color*	limitedParticleColor;

	float	speed;
	int		particleNumber;
	int		limitedParticleNumber;
	bool	playing;
	bool	mouseTracking;
	bool	rainbowMode;

	float	gravityDistance;
	float	lifetime;

	CameraMode cameraMode;
	Shape	shape;

	void initSkybox();
};

#endif