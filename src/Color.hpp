#ifndef COLOR_HPP
#define COLOR_HPP

#include <glm/glm.hpp>

class Color {

public:

	Color(glm::vec3 hsv, float speed);
	
	void toRGB();
	void toHSV();

	glm::vec3 getRGB() const;
	glm::vec3 getHSV() const;

	void setRGB(glm::vec3);
	void setHSV(glm::vec3);

	void rainbow();

private:

	glm::vec3 rgb;
	glm::vec3 hsv;

	float speed;
};

#endif