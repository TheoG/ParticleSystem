#ifndef SHADER_H
# define SHADER_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

# include <string>
# include <fstream>
# include <sstream>
# include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader {

public:
	Shader(const char* vertexPath, const char* fragmentPath);
	Shader();
	~Shader();
	void use() const;

	unsigned int ID;

	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;
	void setVector(const std::string& name, glm::vec3 value) const;
	void setGLMMatrix(const std::string &name, glm::mat4 m) const;

private:
	void checkCompileErrors(unsigned int shader, std::string type);
};

#endif