#include "Color.hpp"
#include <algorithm>
#include <iostream>

Color::Color(glm::vec3 hsv, float speed) : speed(speed) {
	this->hsv = hsv;
	this->toRGB();
}

void Color::toHSV() {
	
	float cMax = std::max(std::max(rgb.x, rgb.y), rgb.z);
	float cMin = std::min(std::min(rgb.x, rgb.y), rgb.z);
	float delta = cMax - cMin;


	if (delta == 0) {
		hsv.x = 0;
	}
	else if (cMax == rgb.x) {
		hsv.x = 60.0f * ((int)((rgb.y - rgb.z) / delta) % 6);
	}
	else if (cMax == rgb.y) {
		hsv.y = 60.0f * (((rgb.z - rgb.x) / delta) + 2);
	}
	else if (cMax == rgb.z) {
		hsv.y = 60.0f * (((rgb.x - rgb.y) / delta) + 4);
	}

	hsv.y = cMax == 0 ? 0 : delta / cMax;
	hsv.z = cMax;
}

void Color::toRGB() {
	float c = hsv.z * hsv.y;
	float x = c * (1 - std::abs(std::fmodf(hsv.x / 60.0f, 2.0f) - 1));
	float m = hsv.z - c;

	if (hsv.x < 60) {
		rgb.x = c;
		rgb.y = x;
		rgb.z = 0;
	}
	else if (hsv.x < 120) {
		rgb.x = x;
		rgb.y = c;
		rgb.z = 0;
	}
	else if (hsv.x < 180) {
		rgb.x = 0;
		rgb.y = c;
		rgb.z = x;
	}
	else if (hsv.x < 240) {
		rgb.x = 0;
		rgb.y = x;
		rgb.z = c;
	}
	else if (hsv.x < 300) {
		rgb.x = x;
		rgb.y = 0;
		rgb.z = c;
	}
	else if (hsv.x < 360) {
		rgb.x = c;
		rgb.y = 0;
		rgb.z = x;
	}

	rgb.x += m;
	rgb.y += m;
	rgb.z += m;
}

void Color::rainbow() {
	hsv.x += speed;
	if (hsv.x > 360) {
		hsv.x = 0;
	}
	this->toRGB();
}

glm::vec3 Color::getHSV() const {
	return hsv;
}

glm::vec3 Color::getRGB() const {
	return rgb;
}


void Color::setHSV(glm::vec3 hsv) {
	this->hsv = hsv;
	this->toRGB();
}

void Color::setRGB(glm::vec3 rgb) {
	this->rgb = rgb;
	this->toHSV();
}