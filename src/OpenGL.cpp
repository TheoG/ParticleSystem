#include "OpenGL.hpp"

#include <iostream>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

OpenGL::OpenGL() {}
OpenGL::~OpenGL() {}

GLuint OpenGL::getVAO(const std::string& bufferName) const {
	return this->VAOs.at(bufferName);
}

GLuint OpenGL::getVBO(const std::string& bufferName) const {
	return this->VBOs.at(bufferName);
}

// Creates a buffer and stores it into a map (OpenGL VBO and VAO)
GLuint OpenGL::createBuffer(const std::string& bufferName, size_t size, int type) {
	GLuint VAO;
	GLuint VBO;

	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, size, 0, type);

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	this->VBOs[bufferName] = VBO;
	this->VAOs[bufferName] = VAO;
	return VBO;
}

void OpenGL::bindBuffer(const std::string& bufferName) {
	
	std::map<std::string, GLuint>::iterator it;
	it = this->VBOs.find(bufferName);
  	if (it != this->VBOs.end()) {
		glBindBuffer(GL_ARRAY_BUFFER, it->second);
	}
}

// Load a cubemap for the skybox
unsigned int OpenGL::loadCubemap(std::vector<std::string> faces) {
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nbChannels;
	for (unsigned int i = 0; i < faces.size(); i++) {
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nbChannels, 0);

		if (data) {
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		}
		else {
			std::cerr << "Error while loading cubemap" << std::endl;
		}
		stbi_image_free(data);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}

void OpenGL::finish() {
	glFinish();
}