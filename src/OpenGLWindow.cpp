#include "OpenGLWindow.hpp"
#include "ExceptionMsg.hpp"
#include "Time.hpp"

OpenGLWindow::OpenGLWindow( int width, int height, std::string const & title ): nanogui::Screen() {
	
	#ifndef __APPLE__
	nanogui::init();
	#endif

	if (!(this->window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr))) {
		throw ExceptionMsg("Failed to create window");
	}

	if (width < 100 || height < 100) {
		throw ExceptionMsg("Window dimensions too small");
	}

	this->width = width;
	this->height = height;

	glfwMakeContextCurrent(this->window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		throw ExceptionMsg("Failed to initialize GLAD");
	}
	this->shaderProgram = Shader("../src/Shaders/base.vert", "../src/Shaders/base.frag");
	this->shaderProgram.use();

	this->shaderProgram.setGLMMatrix("projectionMatrix", this->camera.getProjectionMatrix());
	this->shaderProgram.setGLMMatrix("viewMatrix", this->camera.getViewMatrix()); 

	this->initialize(this->window, true);

	int w, h;
	glfwGetFramebufferSize(window, &w, &h);
	glViewport(0, 0, w, h);
	// glfwSwapInterval(0); // Removes the 60 FPS constraint.
	glfwSwapBuffers(window);

	glfwSetWindowUserPointer(this->window, this);

	this->setCallbacks();
	Time::deltaTime = 0.0f;

	this->particleSystem = new ParticleSystem(this->shaderProgram);
	
	std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;

	this->setupGUI();
}

void OpenGLWindow::setupGUI() {
	this->gui = new GUI(this);
	this->gui->createSettings();
}

OpenGLWindow::~OpenGLWindow() {
	delete this->particleSystem;
	delete this->gui;
	glfwTerminate();
}

// Main loop for the rendering
void OpenGLWindow::loop() {
	float lastFrame = 0.0f;

	while (!glfwWindowShouldClose(this->window) && glfwGetKey(this->window, GLFW_KEY_ESCAPE) != GLFW_PRESS) {

		glfwPollEvents();

		float currentFrame = static_cast<float>(glfwGetTime());
		Time::deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Displays fps in the window title
		this->updateWindowTitle();

		glClearColor(0.01f, 0.01f, 0.01f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		this->camera.processInput(this->window);
		this->shaderProgram.setGLMMatrix("viewMatrix", this->camera.getViewMatrix());
		this->shaderProgram.setInt("nbParticles", this->particleSystem->getParticleNumber());

		// If limited particle and click, toggle a particle at mouse position
		if (this->particleSystem->getLimitedParticleNumber() > 0 && this->particleSystem->getCameraMode() == ParticleSystem::CameraMode::Cursor && glfwGetMouseButton(this->window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
			GLint viewport[4];
			glGetIntegerv(GL_VIEWPORT, viewport);
			GLint height = viewport[3];
			GLint width = viewport[2];
			double xPos, yPos;

			glfwGetCursorPos(window, &xPos, &yPos);

			glm::vec3 worldPosition = this->particleSystem->mouseToWorld(this->camera, width, height, static_cast<float>(xPos), static_cast<float>(yPos));

			this->particleSystem->updateParticleLifeTime(worldPosition);
		}

		this->particleSystem->drawSkybox(this->camera);
		this->particleSystem->drawBuffer();
		glEnable(GL_DEPTH_TEST);

		this->drawWidgets();

		glfwSwapBuffers(this->window);
	}
}

void OpenGLWindow::updateWindowTitle() {
	char		title[128];
	static double	lastTime = 0.0;

	lastTime += Time::deltaTime;

	if (lastTime >= 1.0) {
		sprintf(title, "Particle System - %.3f ms (%d FPS)", Time::deltaTime, static_cast<int>(1000.0f / Time::deltaTime / 1000.0f));
		glfwSetWindowTitle(this->window, title);
		lastTime = 0.0f;
	}
}

Camera& OpenGLWindow::getCamera() {
	return this->camera;
}

Shader& OpenGLWindow::getShader() {
	return this->shaderProgram;
}

GLFWwindow* OpenGLWindow::getWindow() {
	return this->window;
}

ParticleSystem* OpenGLWindow::getParticleSystem() {
	return this->particleSystem;
}

void OpenGLWindow::setCallbacks() {
	glfwSetCursorPosCallback(window, [](GLFWwindow *win, double xPos, double yPos) {
		OpenGLWindow* screen = (OpenGLWindow*)glfwGetWindowUserPointer(win);
		screen->cursorPosCallbackEvent(xPos, yPos);

		switch (screen->getParticleSystem()->getCameraMode()) {
			case ParticleSystem::CameraMode::Cursor:
				glfwSetInputMode(screen->getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
				break;
			case ParticleSystem::CameraMode::FreeCamera:
				glfwSetInputMode(screen->getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
				screen->getCamera().processMouseInput(static_cast<float>(xPos), static_cast<float>(yPos));
				break;
			case ParticleSystem::CameraMode::Gravity:
				glfwSetInputMode(screen->getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);

				GLint viewport[4];
				glGetIntegerv(GL_VIEWPORT, viewport);
				GLint height = viewport[3];
				GLint width = viewport[2];

				screen->getParticleSystem()->computeGravityPosition(screen->getCamera(), width, height, static_cast<float>(xPos), static_cast<float>(yPos));
				break;
		}
	});

	glfwSetMouseButtonCallback(window, [](GLFWwindow *win, int button, int action, int modifiers) {
		OpenGLWindow* screen = (OpenGLWindow*)glfwGetWindowUserPointer(win);
		if (screen->mouseButtonCallbackEvent(button, action, modifiers)) {
			return;
		}

		ParticleSystem* ps = screen->getParticleSystem();

		if (ps->getCameraMode() == ParticleSystem::CameraMode::Gravity) {
			if (action == GLFW_PRESS) {
				if (button == GLFW_MOUSE_BUTTON_1) {
					GLint viewport[4];
					glGetIntegerv(GL_VIEWPORT, viewport);
					GLint height = viewport[3];
					GLint width = viewport[2];
					double xPos, yPos;

					glfwGetCursorPos(win, &xPos, &yPos);
					glm::vec3 worldPosition = ps->mouseToWorld(screen->getCamera(), width, height, static_cast<float>(xPos), static_cast<float>(yPos));
					ps->addGravityPosition(worldPosition);
				}
				else if (button == GLFW_MOUSE_BUTTON_2) {
					screen->getParticleSystem()->popGravityPosition();
				}
			}
		}
	});

	glfwSetKeyCallback(window, [](GLFWwindow *win, int key, int scancode, int action, int mods) {
		OpenGLWindow* screen = (OpenGLWindow*)glfwGetWindowUserPointer(win);
		screen->keyCallbackEvent(key, scancode, action, mods);

		if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
			if (screen->getParticleSystem()->isPlaying()) {
				screen->getParticleSystem()->pause();
			}
			else {
				screen->getParticleSystem()->play();
			}
		}
		else if (key == GLFW_KEY_R && action == GLFW_PRESS) {
			screen->getParticleSystem()->reset();
			screen->getCamera().reset();
		}
		else if ((key == GLFW_KEY_1 || key == GLFW_KEY_KP_1) && action == GLFW_PRESS) {
			screen->getParticleSystem()->setCameraMode(ParticleSystem::CameraMode::Cursor);
		}
		else if ((key == GLFW_KEY_2 || key == GLFW_KEY_KP_2) && action == GLFW_PRESS) {
			screen->getParticleSystem()->setCameraMode(ParticleSystem::CameraMode::Gravity);
		}
		else if ((key == GLFW_KEY_3 || key == GLFW_KEY_KP_3) && action == GLFW_PRESS) {
			screen->getCamera().resetMouse();
			screen->getParticleSystem()->setCameraMode(ParticleSystem::CameraMode::FreeCamera);
		}	
	});

	glfwSetCharCallback(window, [](GLFWwindow *win, unsigned int codepoint) {
		OpenGLWindow* screen = (OpenGLWindow*)glfwGetWindowUserPointer(win);
		screen->charCallbackEvent(codepoint);
	});

	glfwSetDropCallback(window, [](GLFWwindow *win, int count, const char **filenames) {
		OpenGLWindow* screen = (OpenGLWindow*)glfwGetWindowUserPointer(win);
		screen->dropCallbackEvent(count, filenames);
	});

	glfwSetScrollCallback(window, [](GLFWwindow *win, double x, double y) {
		OpenGLWindow* screen = (OpenGLWindow*)glfwGetWindowUserPointer(win);
		screen->scrollCallbackEvent(x, y);

		if (screen->getParticleSystem()->getCameraMode() != ParticleSystem::CameraMode::Gravity) {
			return ;
		}


		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		GLint height = viewport[3];
		GLint width = viewport[2];
		double xPos, yPos;
		glfwGetCursorPos(win, &xPos, &yPos);

		float distance = screen->getParticleSystem()->getGravityDistance();
		distance += static_cast<float>(y) / 100.0f;
		screen->getParticleSystem()->setGravityDistance(distance);

		screen->getParticleSystem()->computeGravityPosition(screen->getCamera(), width, height, static_cast<float>(xPos), static_cast<float>(yPos));
	});

	glfwSetFramebufferSizeCallback(window, [](GLFWwindow *win, int width, int height) {
		OpenGLWindow* screen = (OpenGLWindow*)glfwGetWindowUserPointer(win);
		screen->resizeCallbackEvent(width, height);
		screen->getCamera().resize(width, height);
		screen->shaderProgram.setGLMMatrix("projectionMatrix", screen->camera.getProjectionMatrix());
	});
}


void OpenGLWindow::initOpenGL() {
	if (!glfwInit()) {
		throw ExceptionMsg("Failed to initialize GLFW");
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
}

const int OpenGLWindow::defaultHeight = 845;
const int OpenGLWindow::defaultWidth = 1500;