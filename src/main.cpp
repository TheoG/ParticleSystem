#include "OpenGLWindow.hpp"

#include <iostream>

int	main () {

	#ifdef __APPLE__
	OpenGLWindow::initOpenGL();
	#endif

	OpenGLWindow* w = new OpenGLWindow(OpenGLWindow::defaultWidth, OpenGLWindow::defaultHeight, "Particle System");
	w->loop();

	delete w;
	return (0);
}