#ifndef OPENCL_CLASS_HPP
# define OPENCL_CLASS_HPP

#ifdef WIN32
# include <CL/cl.h>
# else
# include <OpenCL/cl.h>
#endif

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <map>

#include "cl.hpp"

constexpr auto CL_KERNEL_NAME_DEFAULT = "update";
constexpr auto CL_KERNEL_NAME_LIMITED_PARTICLES = "updateLimitedParticles";
constexpr auto CL_KERNEL_NAME_SPHERE = "init_sphere";
constexpr auto CL_KERNEL_NAME_CUBE = "init_cube";
constexpr auto CL_KERNEL_NAME_LIFETIME = "update_lifetime";

class OpenCL {

	public:
		OpenCL();
		~OpenCL();

		cl::BufferGL createBufferFromOpenGL(const std::string& name, GLuint VBO);
		cl::Buffer	createBuffer(void* data, size_t size);

		cl::Kernel getKernel(const std::string& name);
		cl::BufferGL& getBuffer(const std::string& name);
		cl::CommandQueue getCommandQueue() const;

		void createProgram(const std::string& name, const std::string& kernelPath);
	private:

		// Declare only one
		cl::Platform		platform;
		cl::Device			device;
		cl::Context			context;
		cl::CommandQueue	commandQueue;

		// Can have multiple
		std::map<const std::string, cl::Kernel>	kernels;
		std::map<const std::string, cl::BufferGL> buffers;


		std::string		readFile(const std::string& filePath);
};

#endif