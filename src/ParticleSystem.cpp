#ifdef WIN32
#include <Windows.h>
#else
# include <OpenGL/CGLCurrent.h>
#endif

#include "ParticleSystem.hpp"
#include "Time.hpp"

#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

std::string getErrorString(int error);

std::string	readFile(const std::string& filePath) {
	std::string kernelCode;
	std::ifstream kernelFile;

	kernelFile.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
	try {
		kernelFile.open(filePath);
		std::stringstream kernelStream;
		kernelStream << kernelFile.rdbuf();
		kernelFile.close();
		kernelCode = kernelStream.str();
	}
	catch (std::ifstream::failure e) {
		std::cerr << "ERROR::KERNEL FILE_NOT_SUCCESFULLY_READ" << std::endl;
		exit (1);
	}
	return kernelCode;
}

ParticleSystem::ParticleSystem(Shader& shaderProgram): shaderProgram(shaderProgram), gravityDistance(-Camera::defaultCameraPos.z) {
	this->particleNumber = ParticleSystem::DefaultNbParticles;
	this->lifetime = ParticleSystem::DefaultLifetime;
	this->limitedParticleNumber = 0;
	
	this->opengl = new OpenGL();
	this->opencl = new OpenCL();

	GLuint glPositionBufferID = this->opengl->createBuffer(PARTICLE_POSITION_BUFFER_NAME, sizeof(glm::vec4) * (this->particleNumber + this->limitedParticleNumber), GL_DYNAMIC_DRAW);
	GLuint glVelocityBufferID = this->opengl->createBuffer(PARTICLE_VELOCITY_BUFFER_NAME, sizeof(glm::vec4) * (this->particleNumber + this->limitedParticleNumber), GL_DYNAMIC_DRAW);
	
	this->opencl->createBufferFromOpenGL(PARTICLE_POSITION_BUFFER_NAME, glPositionBufferID);
	this->opencl->createBufferFromOpenGL(PARTICLE_VELOCITY_BUFFER_NAME, glVelocityBufferID);

	this->initSkybox();
	
	this->opencl->createProgram(CL_KERNEL_NAME_DEFAULT,  "../src/Kernels/update_particles.cl");
	this->opencl->createProgram(CL_KERNEL_NAME_LIMITED_PARTICLES,  "../src/Kernels/update_particles.cl");
	this->opencl->createProgram(CL_KERNEL_NAME_SPHERE, "../src/Kernels/init_sphere.cl");
	this->opencl->createProgram(CL_KERNEL_NAME_CUBE, "../src/Kernels/init_cube.cl");
	this->opencl->createProgram(CL_KERNEL_NAME_LIFETIME, "../src/Kernels/lifetime_particles.cl");
	
	this->createSphere();

	this->addGravityPosition(glm::vec3(0.f));
		
	this->playing = false;
	this->rainbowMode = true;
	this->mouseTracking = false;

	this->particleColor = new Color(glm::vec3(0, 1, 1), 0.5f);
	this->limitedParticleColor = new Color(glm::vec3(0, 1, 1), .0f);
	this->speed = ParticleSystem::DefaultSpeed;
	this->shape = ParticleSystem::Shape::Sphere;
	this->cameraMode = ParticleSystem::CameraMode::Cursor;
}

ParticleSystem ::~ParticleSystem() {
	delete this->opencl;
	delete this->opengl;
	delete this->particleColor;
	delete this->limitedParticleColor;
	delete this->skyboxShader;
}

void ParticleSystem::initSkybox() {
	float skyboxVertices[] = {
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};

	this->opengl->createBuffer(SKYBOX_BUFFER_NAME, sizeof(skyboxVertices), GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*) 0);
	
	std::vector<std::string> faces;
	faces.push_back("../textures/bkg1_right.png");
	faces.push_back("../textures/bkg1_left.png");
	faces.push_back("../textures/bkg1_top.png");
	faces.push_back("../textures/bkg1_bot.png");
	faces.push_back("../textures/bkg1_front.png");
	faces.push_back("../textures/bkg1_back.png");
	this->cubemapTexture = this->opengl->loadCubemap(faces);

	this->skyboxShader = new Shader("../src/Shaders/skybox.vert", "../src/Shaders/skybox.frag");
	this->skyboxShader->use();
}


void ParticleSystem::reset() {
	GLuint glPositionBufferID = this->opengl->createBuffer(PARTICLE_POSITION_BUFFER_NAME, sizeof(glm::vec4) * (this->particleNumber + this->limitedParticleNumber), GL_DYNAMIC_DRAW);
	GLuint glVelocityBufferID = this->opengl->createBuffer(PARTICLE_VELOCITY_BUFFER_NAME, sizeof(glm::vec4) * (this->particleNumber + this->limitedParticleNumber), GL_DYNAMIC_DRAW);
	
	this->opencl->createBufferFromOpenGL(PARTICLE_POSITION_BUFFER_NAME, glPositionBufferID);
	this->opencl->createBufferFromOpenGL(PARTICLE_VELOCITY_BUFFER_NAME, glVelocityBufferID);

	if (this->shape == Shape::Sphere) {
		this->createSphere();
	}
	else {
		this->createCube();
	}

	this->gravityPositions.clear();
	this->addGravityPosition(glm::vec3(0.f));

	this->gravityDistance = -Camera::defaultCameraPos.z;

	shaderProgram.setVector("gravityPosition", this->getGravityPosition(0));
}

void ParticleSystem::setParticleNumber(int number, int limitedParticles) {
	this->particleNumber = number;
	this->limitedParticleNumber = limitedParticles;
}

int ParticleSystem::getParticleNumber() const {
	return this->particleNumber;
}

int ParticleSystem::getLimitedParticleNumber() const {
	return this->limitedParticleNumber;
}

void ParticleSystem::play() {
	this->playing = true;
}

void ParticleSystem::pause() {
	this->playing = false;
}

bool ParticleSystem::isPlaying() const {
	return this->playing;
}

void ParticleSystem::drawBuffer() {

	if (rainbowMode) {
		particleColor->rainbow();
	}
	shaderProgram.setVector("baseParticleColor", particleColor->getRGB());
	shaderProgram.setVector("limitedParticleColor", limitedParticleColor->getRGB());

	if (this->playing) {
		// If this->numberLimitedParticle is positive, call another kernel
		// else, do regular particle system kernel
		if (this->limitedParticleNumber > 0) {
			this->updateLimitedParticlePositions();
		} else {
			this->updateParticlePositions();
		}
	}
	shaderProgram.setBool("limitedParticles", false);
	shaderProgram.setVector("gravityPosition", this->getGravityPosition(0));

	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable (GL_DEPTH_TEST);
	glDepthFunc (GL_LEQUAL);
	glBindVertexArray(this->opengl->getVAO(PARTICLE_POSITION_BUFFER_NAME));
	glBindBuffer(GL_ARRAY_BUFFER, this->opengl->getVBO(PARTICLE_POSITION_BUFFER_NAME));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);

	glDrawArrays(GL_POINTS, 0, this->particleNumber);

	if (this->limitedParticleNumber > 0) {

		shaderProgram.setBool("limitedParticles", true);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), 0);

		glBindVertexArray(this->opengl->getVAO(PARTICLE_POSITION_BUFFER_NAME));
		glBindBuffer(GL_ARRAY_BUFFER, this->opengl->getVBO(PARTICLE_POSITION_BUFFER_NAME));

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)(sizeof(float) * 4 * this->particleNumber));
		
		glDrawArrays(GL_POINTS, 0, this->limitedParticleNumber);
	}

}

void ParticleSystem::drawSkybox(Camera& cam) const {
	glDepthFunc(GL_LEQUAL);
	this->skyboxShader->use();
	this->skyboxShader->setGLMMatrix("view", glm::mat4(glm::mat3(cam.getViewMatrix())));
	this->skyboxShader->setGLMMatrix("projection", cam.getProjectionMatrix());
	glBindVertexArray(this->opengl->getVAO(SKYBOX_BUFFER_NAME));
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, this->cubemapTexture);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glDepthFunc(GL_LESS); // set depth function back to default
}

void ParticleSystem::createSphere() {

	cl::CommandQueue commandQueue = this->opencl->getCommandQueue();
	cl::Kernel kernel = this->opencl->getKernel(CL_KERNEL_NAME_SPHERE);

	std::vector<cl::Memory> memories;
	memories.push_back(this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	memories.push_back(this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));

	glFinish();
	commandQueue.enqueueAcquireGLObjects(&memories, nullptr, nullptr);

	kernel.setArg(0, this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	kernel.setArg(1, this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));
	kernel.setArg(2, this->particleNumber);

	commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(this->particleNumber), cl::NullRange);

	commandQueue.enqueueReleaseGLObjects(&memories, nullptr, nullptr);
	commandQueue.finish();
}

void ParticleSystem::createCube() {
	cl::CommandQueue commandQueue = this->opencl->getCommandQueue();
	cl::Kernel kernel = this->opencl->getKernel(CL_KERNEL_NAME_CUBE);

	std::vector<cl::Memory> memories;
	memories.push_back(this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	memories.push_back(this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));

	glFinish();
	commandQueue.enqueueAcquireGLObjects(&memories, nullptr, nullptr);

	kernel.setArg(0, this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	kernel.setArg(1, this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));
	kernel.setArg(2, static_cast<int>(std::cbrt(this->particleNumber)));

	commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(this->particleNumber), cl::NullRange);

	commandQueue.enqueueReleaseGLObjects(&memories, nullptr, nullptr);
	commandQueue.finish();
}

void ParticleSystem::updateParticlePositions() {

	cl::CommandQueue commandQueue = this->opencl->getCommandQueue();
	cl::Kernel kernel = this->opencl->getKernel(CL_KERNEL_NAME_DEFAULT);

	std::vector<cl::Memory> memories;

	memories.push_back(this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	memories.push_back(this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));

	glFinish();
	commandQueue.enqueueAcquireGLObjects(&memories, nullptr, nullptr);

	std::vector<glm::vec3> gravityPositions = this->getGravityPositions();

	glm::vec4 gravityPositionsArray[MAX_GRAVITY_POINTS];

	for (unsigned i = 0; i < gravityPositions.size(); i++) {
		gravityPositionsArray[i].x = gravityPositions[i].x;
		gravityPositionsArray[i].y = gravityPositions[i].y;
		gravityPositionsArray[i].z = gravityPositions[i].z;
	}

	cl::Buffer gravityPositionsBuffer = this->opencl->createBuffer(gravityPositionsArray, sizeof(gravityPositionsArray));

	kernel.setArg(0, this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	kernel.setArg(1, this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));
	kernel.setArg(2, gravityPositionsBuffer);
	kernel.setArg(3, glm::vec4(Time::deltaTime * this->speed));
	kernel.setArg(4, static_cast<int>(this->gravityPositions.size()));

	cl::Event event;

	commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, particleNumber, cl::NullRange, nullptr, &event);

	commandQueue.enqueueReleaseGLObjects(&memories, nullptr, nullptr);
	commandQueue.finish();
}

void ParticleSystem::updateLimitedParticlePositions() {
	cl::CommandQueue commandQueue = this->opencl->getCommandQueue();
	cl::Kernel kernel = this->opencl->getKernel(CL_KERNEL_NAME_LIMITED_PARTICLES);

	std::vector<cl::Memory> memories;

	memories.push_back(this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	memories.push_back(this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));

	glFinish();
	commandQueue.enqueueAcquireGLObjects(&memories, nullptr, nullptr);

	std::vector<glm::vec3> gravityPositions = this->getGravityPositions();

	glm::vec4 gravityPositionsArray[MAX_GRAVITY_POINTS];

	for (unsigned i = 0; i < gravityPositions.size(); i++) {
		gravityPositionsArray[i].x = gravityPositions[i].x;
		gravityPositionsArray[i].y = gravityPositions[i].y;
		gravityPositionsArray[i].z = gravityPositions[i].z;
	}

	cl::Buffer gravityPositionsBuffer = this->opencl->createBuffer(gravityPositionsArray, sizeof(gravityPositionsArray));

	kernel.setArg(0, this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	kernel.setArg(1, this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));
	kernel.setArg(2, gravityPositionsBuffer);
	kernel.setArg(3, glm::vec4(Time::deltaTime * this->speed));
	kernel.setArg(4, this->particleNumber);
	kernel.setArg(5, this->limitedParticleNumber);
	kernel.setArg(6, static_cast<int>(this->gravityPositions.size()));

	cl::Event event;

	commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, particleNumber + limitedParticleNumber, cl::NullRange, nullptr, &event);

	commandQueue.enqueueReleaseGLObjects(&memories, nullptr, nullptr);
	commandQueue.finish();
}

void ParticleSystem::updateParticleLifeTime(glm::vec3 worldPosition) {
	cl::CommandQueue commandQueue = this->opencl->getCommandQueue();
	cl::Kernel kernel = this->opencl->getKernel(CL_KERNEL_NAME_LIFETIME);

	std::vector<cl::Memory> memories;
	memories.push_back(this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	memories.push_back(this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));

	glFinish();
	commandQueue.enqueueAcquireGLObjects(&memories, nullptr, nullptr);

	kernel.setArg(0, this->opencl->getBuffer(PARTICLE_POSITION_BUFFER_NAME));
	kernel.setArg(1, this->opencl->getBuffer(PARTICLE_VELOCITY_BUFFER_NAME));
	kernel.setArg(2, this->lifetime);
	kernel.setArg(3, this->particleNumber);
	kernel.setArg(4, this->limitedParticleNumber);
	float worldPositionArray[4] = { worldPosition.x, worldPosition.y, worldPosition.z, 0 };

	kernel.setArg(5, worldPositionArray);
	commandQueue.enqueueNDRangeKernel(kernel, cl::NullRange, 1, cl::NullRange);

	commandQueue.enqueueReleaseGLObjects(&memories, nullptr, nullptr);
	commandQueue.finish();
}

void ParticleSystem::setGravityPosition(int index, glm::vec3 position) {
	this->gravityPositions[index] = position;
}

void ParticleSystem::addGravityPosition(glm::vec3 position) {
	if (this->gravityPositions.size() < MAX_GRAVITY_POINTS) {
		this->gravityPositions.push_back(position);
	}
}

void ParticleSystem::popGravityPosition() {
	if (this->gravityPositions.size() > 1) {
		this->gravityPositions.pop_back();
	}
}

glm::vec3& ParticleSystem::getGravityPosition(int index) {
	return this->gravityPositions[index];
}

std::vector<glm::vec3> ParticleSystem::getGravityPositions() const {
	return this->gravityPositions;
}

void ParticleSystem::toggleRainbowMode(bool toggle) {
	this->rainbowMode = toggle;
}

void ParticleSystem::setParticleColor(glm::vec3 color) {
	this->particleColor->setRGB(color);
	shaderProgram.setVector("baseParticleColor", this->particleColor->getRGB());
}

void ParticleSystem::setLimitedParticleColor(glm::vec3 color) {
	this->limitedParticleColor->setRGB(color);
	shaderProgram.setVector("limitedParticleColor", this->limitedParticleColor->getRGB());
}

glm::vec3 ParticleSystem::getParticleColor() const {
	return this->particleColor->getRGB();
}

void ParticleSystem::setShape(ParticleSystem::Shape shape) {
	this->shape = shape;
}

void ParticleSystem::setSpeed(float speed) {
	this->speed = std::clamp(speed, ParticleSystem::MinSpeed, ParticleSystem::MaxSpeed);

}

bool ParticleSystem::isTrackingMouse() const {
	return this->mouseTracking;
}

void ParticleSystem::toggleMouseTrack(bool toggle) {
	this->mouseTracking = toggle;
}

float ParticleSystem::getGravityDistance() const {
	return this->gravityDistance;
}

void ParticleSystem::setGravityDistance(float distance) {
	this->gravityDistance = distance;
}

ParticleSystem::CameraMode ParticleSystem::getCameraMode() const {
	return this->cameraMode;
}

void ParticleSystem::setCameraMode(ParticleSystem::CameraMode cameraMode) {
	std::string mode;
	switch (cameraMode) {
		case ParticleSystem::CameraMode::FreeCamera:
			mode = "free camera";
			break;
		case ParticleSystem::CameraMode::Gravity:
			mode = "gravity";
			break;
		case ParticleSystem::CameraMode::Cursor:
			mode = "cursor";
			break;
	}
	this->cameraMode = cameraMode;
}

void ParticleSystem::computeGravityPosition(Camera& cam, int width, int height, float xPos, float yPos) {

	glm::vec3 gravityPosition = this->mouseToWorld(cam, width, height, xPos, yPos);

	// Get latest index in the vector
	this->setGravityPosition(this->gravityPositions.size() - 1, gravityPosition);
}

glm::vec3 ParticleSystem::mouseToWorld(Camera& cam, int width, int height, float xPos, float yPos) {
	float x = (2.0f * xPos) / width - 1.0f;
	float y = 1.0f - (2.0f * yPos) / height;
	float z = 1.5f;

	glm::vec3 ray_nds = glm::vec3(x, y, z);
	
	glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0f, 1.0f);

	glm::vec4 ray_eye = glm::inverse(cam.getProjectionMatrix()) * ray_clip;
	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0f, 0.0f);

	glm::vec3 ray_wor = glm::inverse(cam.getViewMatrix()) * ray_eye;
	glm::normalize(ray_wor);

	return cam.getPosition() + ray_wor * this->gravityDistance;
}

float ParticleSystem::getLifetime() const {
	return this->lifetime;
}

void ParticleSystem::setLifetime(float lifetime) {
	this->lifetime = std::clamp(lifetime, ParticleSystem::MinLifetime, ParticleSystem::MaxLifetime);
}

const float ParticleSystem::MinSpeed = 0.01f;
const float ParticleSystem::MaxSpeed = 1.0f;
const float ParticleSystem::DefaultSpeed = 0.15f;

const float ParticleSystem::MinLifetime = 0.5f;
const float ParticleSystem::MaxLifetime = 10.0f;
const float ParticleSystem::DefaultLifetime = 3.0f;

const int ParticleSystem::MinNbParticles = 100;
const int ParticleSystem::MaxNbParticles = 3'000'000;
const int ParticleSystem::DefaultNbParticles = 1'000'000;

const int ParticleSystem::MinNbLimitedParticles = 0;
const int ParticleSystem::MaxNbLimitedParticles = 10'000;
const int ParticleSystem::DefaultNbLimitedParticles = 0;

const int ParticleSystem::MaxGravityPoints = 2;
