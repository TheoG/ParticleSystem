#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

# include <GLFW/glfw3.h>

#include <iostream>

class Camera
{
public:
	Camera();
	~Camera();

	const glm::mat4& getViewMatrix() const;
	const glm::mat4& getProjectionMatrix() const;
	glm::vec3 getPosition() const;

	void reset();
	void resetMouse();

	void resize(int x, int y);
	void processInput(GLFWwindow* window);
	void processMouseInput(float newX, float newY);
	void updateViewMatrix();

	bool isFirstMouseMovement() const;

	static const glm::vec3 defaultCameraPos;
private:
	glm::mat4 view;
	glm::mat4 projection;

	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 worldUp;
	glm::vec3 lookAt;

	glm::vec2 lastCursorPos;
	
	float pitch;
	float yaw;
	bool firstMouseMovement;
};

#endif