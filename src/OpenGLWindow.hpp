#ifndef OPENGL_WINDOW_HPP
# define OPENGL_WINDOW_HPP

# include <glad/glad.h>
# include <GLFW/glfw3.h>
# include <string>
# include <nanogui/nanogui.h>


# include "Shader.hpp"
# include "Camera.hpp"
# include "ParticleSystem.hpp"

class GUI;

class OpenGLWindow: public nanogui::Screen {

public:
	OpenGLWindow( int width, int height, std::string const & title );
	virtual ~OpenGLWindow( void );

	void loop();

	Camera& getCamera();
	Shader& getShader();
	GLFWwindow* getWindow();
	ParticleSystem* getParticleSystem();

	void setupGUI();

	static void initOpenGL();
	static const int defaultWidth;
	static const int defaultHeight;

private:
	GLFWwindow	*window;
	GUI*		gui;
	Shader		shaderProgram;
	int			width;
	int			height;

	Camera		camera;


	void updateWindowTitle();
	void setCallbacks();


	ParticleSystem*	particleSystem;
};

# include "GUI.hpp"

#endif