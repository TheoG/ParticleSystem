#include "OpenGLWindow.hpp"
#include "Camera.hpp"
#include <algorithm>

#include "Time.hpp"

Camera::Camera(): firstMouseMovement(true) {
	this->reset();

	this->projection = glm::perspective(glm::radians(45.0f), static_cast<float>(OpenGLWindow::defaultWidth) / static_cast<float>(OpenGLWindow::defaultHeight), 0.1f, 1000.0f);
	this->lastCursorPos.x = static_cast<float>(OpenGLWindow::defaultWidth) / 2.0f;
	this->lastCursorPos.y = static_cast<float>(OpenGLWindow::defaultHeight) / 2.0f;
	this->pitch = this->yaw = 0.0f;
}

Camera::~Camera() {}

void Camera::resize(int x, int y) {
	this->projection = glm::perspective(glm::radians(45.0f), static_cast<float>(x) / static_cast<float>(y), 0.1f, 1000.0f);
}

void Camera::reset() {
	this->position	= Camera::defaultCameraPos;
	this->front		= glm::vec3(0.0f,  0.0f,  1.0f);
	this->up		= glm::vec3(0.0f,  1.0f,  0.0f);
	this->worldUp 	= this->up;
	this->lookAt	= glm::vec3(0.0f,  0.0f,  0.0f);
	this->resetMouse();
	this->updateViewMatrix();
}

void Camera::resetMouse() {
	this->firstMouseMovement = true;
	this->lastCursorPos.x = static_cast<float>(OpenGLWindow::defaultWidth) / 2.0f;
	this->lastCursorPos.y = static_cast<float>(OpenGLWindow::defaultHeight) / 2.0f;
	this->pitch = this->yaw = 0.0f;
}

bool Camera::isFirstMouseMovement() const {
	return this->firstMouseMovement;
}

const glm::mat4& Camera::getViewMatrix() const {
	return this->view;
}

const glm::mat4& Camera::getProjectionMatrix() const {
	return this->projection;
}

glm::vec3 Camera::getPosition() const {
	return this->position;
}

void Camera::updateViewMatrix() {
	this->view = glm::lookAt(this->position, this->position + this->front, this->up);
}

void Camera::processInput(GLFWwindow *window) {
	float cameraSpeed = 2.5f * Time::deltaTime;

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		this->position += cameraSpeed * this->front;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		this->position -= cameraSpeed * this->front;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		this->position -= glm::normalize(glm::cross(this->front, this->up)) * cameraSpeed;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		this->position += glm::normalize(glm::cross(this->front, this->up)) * cameraSpeed;
	}
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
		this->position -= cameraSpeed * this->worldUp;
	}
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
		this->position += cameraSpeed * this->worldUp;
	}

	this->updateViewMatrix();
}

void Camera::processMouseInput(float newX, float newY) {

	if (this->firstMouseMovement) {
		this->lastCursorPos = glm::vec2(newX, newY);
		this->firstMouseMovement = false;
	}

	float xOffset = newX - this->lastCursorPos.x;
	float yOffset = this->lastCursorPos.y - newY;

	this->lastCursorPos = glm::vec2(newX, newY);

	float sensitivity = 0.05f;
	xOffset *= sensitivity;
	yOffset *= sensitivity;

	this->yaw += xOffset;
	this->pitch += yOffset;

	this->pitch = std::clamp(this->pitch, -89.0f, 89.0f);

	this->front.x = -cos(glm::radians(this->pitch)) * sin(glm::radians(this->yaw));
	this->front.z = cos(glm::radians(this->pitch)) * cos(glm::radians(this->yaw));
	this->front.y = sin(glm::radians(this->pitch));

	this->front = glm::normalize(front);
	this->updateViewMatrix();
}

const glm::vec3 Camera::defaultCameraPos = glm::vec3(0.0f, 0.0f, -3.0f);