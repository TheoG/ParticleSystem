#include "GUI.hpp"

#include <string>

GUI::GUI(OpenGLWindow* screen): screen(screen) {}

GUI::~GUI() {}

void GUI::createSettings() {
	int screenWidth, screenHeight;
	glfwGetWindowSize(screen->getWindow(), &screenWidth, &screenHeight);

	// =================≈============== COLOR GUI ===============================
	// Color Window
	nanogui::Window *colorWindow = new nanogui::Window(screen, "Colors");
	colorWindow->setFixedWidth(225);
	colorWindow->setPosition(nanogui::Vector2i(screenWidth - 225 - 15, 15));
	colorWindow->setLayout(new nanogui::GroupLayout());

	// Color checkbox and color wheel
	nanogui::CheckBox* rainbowMode = new nanogui::CheckBox(colorWindow, "Rainbow");
	nanogui::ColorWheel* colorWheel = new nanogui::ColorWheel(colorWindow, nanogui::Color());

	rainbowMode->setChecked(true);
	rainbowMode->setCallback([this, colorWheel](bool toggle) {
		colorWheel->setVisible(!toggle);
		this->screen->getParticleSystem()->toggleRainbowMode(toggle);
		if (!toggle) {
			glm::vec3 pCol = this->screen->getParticleSystem()->getParticleColor();
			colorWheel->setColor(nanogui::Color(pCol.x, pCol.y, pCol.z, 1.0f));
		}
		screen->performLayout();
	});
	colorWheel->setCallback([this](nanogui::Color newColor) {

		this->screen->getParticleSystem()->setParticleColor(glm::vec3(newColor.r(), newColor.g(), newColor.b()));
	});
	colorWheel->setVisible(false);

	// Color wheel for limited particles
	auto limitedParticleColorLabel = new nanogui::Label(colorWindow, "Limited Particles Color", "sans-bold");
	nanogui::ColorWheel* limitedParticlesColorWheel = new nanogui::ColorWheel(colorWindow, nanogui::Color(255, 0, 0, 0));
	limitedParticlesColorWheel->setCallback([this](nanogui::Color newColor) {
		this->screen->getParticleSystem()->setLimitedParticleColor(glm::vec3(newColor.r(), newColor.g(), newColor.b()));
	});
	// =============================== COLOR GUI ===============================
		
	// ============================ REGULAR SETTINGS ===========================
	// Regular settings window
	nanogui::Window *particleSettingsWindow = new nanogui::Window(screen, "Particle Settings");
	particleSettingsWindow->setFixedWidth(225);
	particleSettingsWindow->setPosition(nanogui::Vector2i(15, 15));
	particleSettingsWindow->setLayout(new nanogui::GroupLayout());

	// Reset shape, and all
	nanogui::Label *resetLabel = new nanogui::Label(particleSettingsWindow, "Reset", "sans-bold");
	resetLabel->setFontSize(20);
	nanogui::Widget *panel = new nanogui::Widget(particleSettingsWindow);
	panel->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
		nanogui::Alignment::Fill, 2, 10));

	nanogui::Button *initSphere = new nanogui::Button(panel, "Sphere");
	initSphere->setFixedWidth(70);
	initSphere->setCallback([this]() {
		this->screen->getParticleSystem()->setShape(ParticleSystem::Shape::Sphere);
		this->screen->getParticleSystem()->reset();
	});
	initSphere->setTooltip("Reset particles as sphere");
	nanogui::Button *initCube = new nanogui::Button(panel, "Cube");
	initCube->setFixedWidth(70);
	initCube->setCallback([this]() {
		this->screen->getParticleSystem()->setShape(ParticleSystem::Shape::Cube);
		this->screen->getParticleSystem()->reset();
	});
	initCube->setTooltip("Reset particles as cube");

	// Particle Speed
	new nanogui::Label(particleSettingsWindow, "Speed", "sans-bold");
	
	panel = new nanogui::Widget(particleSettingsWindow);
	panel->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
		nanogui::Alignment::Maximum, 2, 0));

	nanogui::Slider *speedSlider = new nanogui::Slider(panel);
	speedSlider->setRange(std::pair<float, float>(ParticleSystem::MinSpeed, ParticleSystem::MaxSpeed));
	speedSlider->setValue(ParticleSystem::DefaultSpeed);
	speedSlider->setFixedWidth(125);

	nanogui::TextBox *textBox = new nanogui::TextBox(panel);
	textBox->setFixedSize(nanogui::Vector2i(50, 25));
	std::string defaultSpeedString = std::to_string(ParticleSystem::DefaultSpeed);
	defaultSpeedString.resize(4);
	textBox->setValue(defaultSpeedString);

	speedSlider->setCallback([this, textBox](float speed) {
		this->screen->getParticleSystem()->setSpeed(speed);
		std::string defaultSpeedString = std::to_string(speed);
		defaultSpeedString.resize(4);
		textBox->setValue(defaultSpeedString);
	});


	// Particle Number
	new nanogui::Label(particleSettingsWindow, "Number of Particles", "sans-bold");

	panel = new nanogui::Widget(particleSettingsWindow);
	panel->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
		nanogui::Alignment::Maximum, 2, 0));

	nanogui::Slider *nbParticlesSlider = new nanogui::Slider(panel);
	nbParticlesSlider->setRange(std::pair<float, float>(static_cast<float>(ParticleSystem::MinNbParticles), static_cast<float>(ParticleSystem::MaxNbParticles)));
	nbParticlesSlider->setValue(static_cast<float>(ParticleSystem::DefaultNbParticles));
	nbParticlesSlider->setFixedWidth(100);

	textBox = new nanogui::TextBox(panel);
	textBox->setFontSize(15);
	defaultSpeedString = std::to_string(ParticleSystem::DefaultNbParticles);
	textBox->setValue(defaultSpeedString);

	nbParticlesSlider->setCallback([this, textBox](int number) {
		std::string defaultNbString = std::to_string(number);
		textBox->setValue(defaultNbString);
	});
	// ============================ REGULAR SETTINGS ===========================	


	// =============================== LIMITED LIFETIME PARTICLES ===============================

	auto limitedParticleTitle = new nanogui::Label(particleSettingsWindow, "Limited Particles", "sans-bold");
	limitedParticleTitle->setFontSize(20);

	auto checkbox = new nanogui::CheckBox(particleSettingsWindow, "Limited Particles");

	checkbox->setChecked(false);
	
	auto numberLimitedParticleLabel = new nanogui::Label(particleSettingsWindow, "Number of Limited Lifetime Particles", "sans-bold");

	panel = new nanogui::Widget(particleSettingsWindow);
	panel->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
		nanogui::Alignment::Maximum, 2, 0));

	nanogui::Slider *nbTmpParticlesSlider = new nanogui::Slider(panel);
	nbTmpParticlesSlider->setRange(std::pair<float, float>(static_cast<float>(ParticleSystem::MinNbLimitedParticles), static_cast<float>(ParticleSystem::MaxNbLimitedParticles)));
	nbTmpParticlesSlider->setValue(static_cast<float>(ParticleSystem::DefaultNbLimitedParticles));
	nbTmpParticlesSlider->setFixedWidth(100);

	auto nbLimitedParticlesTextBox = new nanogui::TextBox(panel);
	nbLimitedParticlesTextBox->setFixedSize(nanogui::Vector2i(65, 20));
	nbLimitedParticlesTextBox->setFontSize(15);
	defaultSpeedString = std::to_string(ParticleSystem::DefaultNbLimitedParticles);
	nbLimitedParticlesTextBox->setValue(defaultSpeedString);

	nbTmpParticlesSlider->setCallback([this, nbLimitedParticlesTextBox](int number) {
		std::string defaultNbString = std::to_string(number);
		nbLimitedParticlesTextBox->setValue(defaultNbString);
	});

	auto lifetimeLabel = new nanogui::Label(particleSettingsWindow, "Lifetime", "sans-bold");

	panel = new nanogui::Widget(particleSettingsWindow);
	panel->setLayout(new nanogui::BoxLayout(nanogui::Orientation::Horizontal,
		nanogui::Alignment::Maximum, 2, 0));

	nanogui::Slider *lifetimeSlider = new nanogui::Slider(panel);
	lifetimeSlider->setRange(std::pair<float, float>(static_cast<float>(ParticleSystem::MinLifetime), static_cast<float>(ParticleSystem::MaxLifetime)));
	lifetimeSlider->setValue(static_cast<float>(ParticleSystem::DefaultLifetime));
	lifetimeSlider->setFixedWidth(100);

	auto lifetimeTextBox = new nanogui::TextBox(panel);
	lifetimeTextBox->setFixedSize(nanogui::Vector2i(65, 20));
	lifetimeTextBox->setFontSize(15);
	defaultSpeedString = std::to_string(ParticleSystem::DefaultLifetime);
	lifetimeTextBox->setValue(defaultSpeedString);

	lifetimeSlider->setCallback([this, lifetimeTextBox](float number) {
		std::string defaultNbString = std::to_string(number);
		lifetimeTextBox->setValue(defaultNbString);
		this->screen->getParticleSystem()->setLifetime(number);
	});

	// Toggle limited lifetime particles
	checkbox->setCallback([this, numberLimitedParticleLabel, nbTmpParticlesSlider, nbLimitedParticlesTextBox, lifetimeLabel, lifetimeSlider, lifetimeTextBox, limitedParticlesColorWheel, limitedParticleColorLabel](bool toggle) {
		nbTmpParticlesSlider->setVisible(toggle);
		nbLimitedParticlesTextBox->setVisible(toggle);
		numberLimitedParticleLabel->setVisible(toggle);
		lifetimeSlider->setVisible(toggle);
		lifetimeTextBox->setVisible(toggle);
		lifetimeLabel->setVisible(toggle);
		limitedParticlesColorWheel->setVisible(toggle);
		limitedParticleColorLabel->setVisible(toggle);
		this->screen->performLayout();
	});

	// =============================== LIMITED LIFETIME PARTICLES ===============================



	// =============================== APPLY BUTTON ===============================
	nanogui::Button *applyNbParticles = new nanogui::Button(particleSettingsWindow, "Apply Particles Number");
	applyNbParticles->setCallback([this, nbParticlesSlider, nbTmpParticlesSlider]() {
		const int nbParticles = static_cast<int>(nbParticlesSlider->value());
		const int nbLimitedLifetimeParticles = static_cast<int>(nbTmpParticlesSlider->value());
		this->screen->getParticleSystem()->setParticleNumber(nbParticles, nbLimitedLifetimeParticles);
		this->screen->getParticleSystem()->reset();
	});
	// =============================== APPLY BUTTON ===============================

	// Default values
	nbTmpParticlesSlider->setVisible(false);
	nbLimitedParticlesTextBox->setVisible(false);
	numberLimitedParticleLabel->setVisible(false);
	lifetimeSlider->setVisible(false);
	lifetimeTextBox->setVisible(false);
	lifetimeLabel->setVisible(false);
	limitedParticlesColorWheel->setVisible(false);
	limitedParticleColorLabel->setVisible(false);

	screen->performLayout();
}
